[[_TOC_]]

Once all the step described below have been validated, remove this file from the project created from this template.

# (DevOps) How-To setup the project
Use this project as template (the enclosing subgroup is already configured as _gitlab project template source_ 
one lever upper in the group hierarchy), and create a GCP project, with 1 bucket, and 2 service accounts (one 
read-only for all branches, and one with restricted permissions only for the branch with approvals required), 
setting variables appropiately to have them active on default branches (read-only SA)  or protected branches 
requiring approvals before merging.


## GCP project settings

- [ ]  (GCP console) Create a new dedicated GCP project
  - [ ]  make sure the project it's linked to a billing account

- [ ]  (GCP console) activate the `Cloud Storage`,  `Compute Engine`, `Service API`,  `Cloud Resource Manager API` (if not, links may be provided in the CI when it fails)
   - [ ]  `Cloud Storage` https://console.cloud.google.com/storage 
   - [ ]  `Compute Engine`https://console.cloud.google.com/apis/api/compute.googleapis.com 
   - [ ]  `Service API`https://console.cloud.google.com/apis/api/serviceusage.googleapis.com
   - [ ]  `Cloud Resource Manager API` https://console.cloud.google.com/apis/library/cloudresourcemanager.googleapis.com
   - [ ]  `Identity and Access Management (IAM) API` https://console.cloud.google.com/apis/library/iam.googleapis.com (since module `2.2.0`)

- [ ] (GCP console) Create a GCP Cloud Storage bucket for storing the terraform state, 
  * https://console.cloud.google.com/storage/create-bucket
  *  _Tip_: you can use https://gitlab.com/tezos-infra/iac/terraform2/tf-gcp-state-storage-bucket 

- [ ] (git file) Edit the name of the bucket in `~/backend.tf` 
  *  `    bucket = "<BUCKET_NAME>`
  *  _Note_: this cannot be a variable.

- [ ] (GCP console) Create a custom GCP Role titled `Custom - Terraform state bucket access` and named `CustomBucketReadWrite`, adding the following permissions:
```
    storage.buckets.create
    storage.buckets.list
    storage.objects.list
    storage.objects.get
    storage.objects.create
    storage.objects.delete
    storage.objects.update
```
  <!-- * _Note/Todo_: this is to be done for each GCP project to bypass the missing permissions to create folders and  factorize configurations once for all enclosed projects. Could be factorized in 1 single Role and role created at folder level if all similar project were moved under a common folder. -->

<!-- cf https://cloud.google.com/storage/docs/access-control/iam-permissions 
https://cloud.google.com/docs/terraform/resource-management/store-state
-->

- [ ] (GCP console) Create the _read-write_ GCP Service Account, 
  - suggested name: `terraform-readwrite`
  - suggested Description: _terraform read+write (apply) from the CI from <url of the gitlab project>_
  [ ] Add following roles: 
     [ ] role `Compute Admin` to allow the creation of compute instances
     [ ] role previously created `Custom - Terraform state bucket access` to read/write the terraform state
     [ ] role  `Create Service Accounts` (since module `2.2.0`) to create the read-only service account associated with each compute instance
     [ ] role  `Service Account Admin`   (since module `2.2.0`) to manage Service accounts attached to each compute instance
   <!--  [ ] role  `Service Account User`    (since module `2.2.0`) to test, but probably superseeded by Service Account Admin-->
  [ ] create JSON keys and store it safely (we'll upload it to gitlab in the next steps)

- [ ] (GCP console) Create the _read-only_ GCP Service Account
  - suggested name: `terraform-readonly` 
  - suggested Description:  _terraform read-only (plan) from the CI from <url of the gitlab project>_
  - with only role:  `Viewer`
  - [ ] create JSON keys and store it safely

- [ ] (GCP console)  DNS capabilities 
  - Go to the IAM page of the project referenced under `var.dns_hub_project` (defaults to `nl-dns-hub`)
    - [ ] Grant access as `Viewer` (only) to the Service Account `terraform-readonly`
    - [ ] Grant access as `Editor` <!-- TODO : find more precise permission set --> to the Service Account with write permissions (`terraform-readwrite`)

## Gitlab project settings workflow

- [ ] (Gitlab Project) Under `Deploy>Environment`, create an Environment with the exact name `default` from `https://gitlab.com/<project-path>/-/environments` (used to differentiate which service account will be used)

- [ ] (Gitlab Project settings > CI/CD Variables) Set the following variables in gitlab CI settings

* :warning: default variables for google provider, shouldn't be changed.
* :warning: `GCLOUD_KEYFILE_JSON` is used twice, READ-WRITE service account for protected branches on the `default` environment, and READ-only service account (for branches not protected), `*` evironment . It needs to fully run once on the `default` before setting the READ-only.


- [ ] `GCLOUD_PROJECT`
```
* Key: 
GCLOUD_PROJECT
* Description: 
Google Cloud project ID where to deploy
* Type: variable, NOT protected, masked, NOT expanded
* Environment: '* (default)'
* Value:
<ID name of your GCP project>
```
  - [ ] `GCLOUD_REGION`
```
* Key: 
GCLOUD_REGION
* Description: 
Google Cloud region where to deploy the servers
* Type: variable, NOT protected, masked, NOT expanded
Environment: '* (default)'
* Value:
<region, e.g. europe-west1>
```
- [ ] `GCLOUD_ZONE`
```
* Key: 
GCLOUD_ZONE
* Description:
Google Cloud region zone where to deploy
* Type: variable, NOT protected, masked, NOT expanded
Environment: '* (default)'
* Value:
<zone, e.g. europe-west1-c>
```
- [ ] `GCLOUD_KEYFILE_JSON` (same name but for read-write service account), for the Environment named `default`, protected
```
* Key: 
GCLOUD_KEYFILE_JSON
* Description: 
Read-Write Google Cloud Service Account JSON key.
* Type: 
  * file
  * Protected
  * NOT masked (KO)
  * NOT expanded
Environment: default
* Value:
<content of the JSON file>
```
<!-- pause here and make sure to run the pipeline once fully to trigger the initial terraform state -->

- [ ] `GCLOUD_KEYFILE_JSON`  (same name but for read-only service account), for all environments (`*`), not protected

:warning: run the pipeline in read-write mode first to make sure the initial tf state is created;
until this, the terraform plan jobs and other jobs using this read-only service account will fail.

```
* Key: 
GCLOUD_KEYFILE_JSON
* Description: 
Read-Only Google Cloud Service Account JSON key.
* Type:
  * file
  * NOT Protected
  * NOT masked (KO)
  * NOT expanded* Environment: '* (default)'
* Value:
<content of the JSON file>
```
<!-- (cf. https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/provider_reference#authentication-configuration ) -->

  * _Note_: the variable `GCLOUD_KEYFILE_JSON` is not duplicated since the environments are different :warning:


- [ ] (Project settings > Merge-Requests) Edit Merge-Request settings so that:
    * require at least 1 approval
    * approvers are (at least) an Eng. Mgr. (EM) and the Head of Eng. (HoE) as fallback

* _Note_: the `CODEOWNERS` file adds the DevOps as required validators for CI files modification to avoid merging modification that would impair the CI or GCP projects Service Accounts.

- [ ] (end-to-end testing) The first run will create the basic network and a server `test` from the file`test_initial.tf`. Remove this server once everything runs correctly (allows to test the correct deletion)


# How-To: Automatic Apply on default branch
<!-- Once satisfied with all the settings -->
You can activate automatic run of the `apply` job on the default branch by setting the following CI/CD variable (Gitlab Project settings > CI/CD Variables) 

```
* Key: 
TF_AUTO_DEPLOY
* Description: 
Apply directly without manual trigger
* Type: file, protected, NOT masked (KO), NOT expanded
* Environment: '* (default)'
* Value:
true
```

# How-To: debug metadata Startup script
* from the GCP console, compute engine, find the culprit VM 
* open an SSH terminal (directly or from GCP) and explore the output of the startup script (note: certain distro do not support login through GCP console, eg. Fedora)

```
# cf. https://cloud.google.com/compute/docs/instances/startup-scripts/linux#accessing-metadata
## re-run the metatada (implies that you're logged in...)
sudo google_metadata_script_runner startup
## logs
sudo journalctl -u google-startup-scripts.service
```

* make sure which version of the terraform module is used in case the script has changed:
 `https://gitlab.com/nomadic-labs/sandboxes/tf-gcp-sandbox-module/-/blob/main/scripts/startup_script.tfpl?ref_type=heads`

