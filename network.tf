# network.tf

# Create a network
resource "google_compute_network" "ipv6net" {
  provider                = google
  name                    = "ipv6net"
  auto_create_subnetworks = false
}

# Create a subnet with IPv6 capabilities
resource "google_compute_subnetwork" "ipv6subnet" {
  provider         = google
  name             = "ipv6subnet"
  network          = google_compute_network.ipv6net.self_link
  ip_cidr_range    = "11.0.0.0/8"
  stack_type       = "IPV4_IPV6"
  ipv6_access_type = "EXTERNAL"
}

# Allow SSH from all IPs (insecure, but ok for a DMZ)
resource "google_compute_firewall" "firewall" {
  provider = google
  name     = "firewall"
  network  = google_compute_network.ipv6net.name

  allow {
    protocol = "icmp"
  }

  source_ranges = ["0.0.0.0/0"]
  allow {
    protocol = "tcp"
    ports    = ["22"]
    # ports for      ssh,  https, http, octez_rcp, octez_metrics
    #    ports    = ["22", "443", "80", "8732",    "9732"]
  }
}
