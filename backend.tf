# backend on GCS

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "5.10.0"
    }
  }
  backend "gcs" {
    bucket = "<bucket-name, eg. PROJECT_NAME-tfstate>"
    prefix = "terraform/state"
  }
}
