## Search and replace "test" => your server name (avoid '-' and '_')
## copy this file to test.tf

module "server_test" {
  # cf. https://gitlab.com/nomadic-labs/sandboxes/tf-gcp-sandbox-module/-/infrastructure_registry
  source  = "gitlab.com/nomadic-labs/tf-gcp-sandbox-module/local"
  version = "2.2.1" # cf. https://gitlab.com/nomadic-labs/sandboxes/tf-gcp-sandbox-module/-/tags

  # Instance name
  instance_name = "test"

  # Machine type, cf. https://cloud.google.com/compute/docs/machine-types"
  machine_type = "e2-standard-2"

  # OS Image, cf. https://gitlab.com/nomadic-labs/sandboxes README for tested values
  image = "ubuntu-os-cloud/ubuntu-2310-mantic-amd64-v20240319"

  # ssh_username: ssh keys with be added to this user (default : nomadic)
  ssh_username = "nomadic"

  # gitlab handles of users to add, from https://gitlab.com/<user>.keys
  gitlab_users = ["yrg"]

  network_self_link    = google_compute_network.ipv6net.self_link
  subnetwork_self_link = google_compute_subnetwork.ipv6subnet.self_link

  # in GB, Book disk should be at least the size of your image
  boot_disk_size = 21

  # in GB, mounted on /opt
  attached_disk_size = 100

  # user_setup_script: commands run by root at startup
  user_setup_script = <<EOH
echo "hello world" >> /tmp/hello_world.txt

EOH

  ## DNS : (defaults to "${var.instance_name}-sandbox.nomadic-labs.eu") 
  ## zone names available: "nomadic-labs-eu" "octez-tech" "octez-online"
  # dns_zone_name          = "nomadic-labs-eu"
  # use_custom_record_name = true
  # custom_record_name     = "" 
}

output "server_test_ssh" {
  description = "ssh command to connect to the server's address if your ssh key is in the list"
  value       = format("ssh %s@%s", module.server_test.ssh_username, trimsuffix(module.server_test.instance_dns_name, "."))
}

output "server_test_IPv4" {
  description = "Ephemeral IP v4 allocated to this server"
  value       = module.server_test.ipv4_address
}

output "server_test_IPv6" {
  description = "IP v6 allocated to this server"
  value       = module.server_test.ipv6_address
}
